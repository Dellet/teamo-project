const {src, dest, parallel, watch, series} = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();


function browser() {
    browserSync.init({
        server: {
            baseDir: "./public/"
        },
        notify: false,
    })
}

function watchFiles() {
    watch("./public/sass/**/*.scss", css);
    watch("./public/index.html").on('change', browserSync.reload);
    watch("./public/js/main.js").on('change', browserSync.reload);
}

function css() {
    return src("./public/sass/**/*.scss")
        .pipe(sass())
        .pipe(dest("./public/css/"))
        .pipe(browserSync.stream());
}

exports.css = css;
exports.default = series(
    series(css),
    parallel(browser, watchFiles)
);