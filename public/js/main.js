'use strict';

$(document).ready(function() {
    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if(isSafari) {
        //For iPhone and Andriod To remove Address bar when viewing website on Safari Mobile
        // When ready...
        window.addEventListener("load",function() {
          // Set a timeout...
          setTimeout(function(){
            // Hide the address bar!
            window.scrollTo(0, 1);
          }, 0);
        });
    }

    // set real screen height
    document.querySelector('body').style.setProperty('--app-height', window.innerHeight + 'px');

    // Intersection Observer for "tabs" block positioning
    {
        let tabs = $('.tabs')
        const elements = []
        for (let i = 0; i < 5; i++) {
            const element = document.querySelector(`.observe-page-${i}`);
            elements.push(element)
        }
        let options = {
            root: document.querySelector('#scrollArea'),
            rootMargin: "5px",
            threshold: 0.1
        };
        const callback = (entries, observer) => {
            entries.forEach((entry, index) => {
                const { target } = entry;
                if (entry.isIntersecting) {

                    if (entry.target.classList.contains("observe-page-0")) {
                        // console.log(entry.intersectionRatio)
                        tabs.css('position', 'fixed');
                        tabs.css('bottom', '0px');
                        $(".diagonal-box").removeClass('translateDown')
                        // console.log('screen1')
                    } else if (entry.target.classList.contains("observe-page-1")) {
                        // console.log(entry.intersectionRatio)
                        tabs.removeClass('fullscreen');
                        tabs.css('position', 'relative');
                        $(".diagonal-box").addClass('translateDown')
                        $("#slideBullets").removeClass('show')

                    } else if (entry.target.classList.contains("observe-page-2")) {
                        tabs.css('display', 'block');
                        tabs.css('visibility', 'visible')
                        tabs.addClass('fullscreen');

                    } else if (entry.target.classList.contains("observe-page-3")) {
                        tabs.css('visibility', 'hidden')
                        tabs.css('display', 'none');
                    }
                    // target.classList.add("is-visible");
                }
            });
        };
        // start observing elements
        const observer = new IntersectionObserver(callback, options);
        elements.forEach((el) => observer.observe(el));
    }

    // Intersection Observer for slide dots
    {
        const swipePages = Array.from(document.querySelectorAll('.observed-swipe-slide'));
        let options = {
            root: document.querySelector('#scrollArea'),
            rootMargin: "5px",
            threshold: 0.5
        };
        const callback = (entries, observer) => {
            // console.log(elements)
            entries.forEach((entry) => {

                let {
                    target
                } = entry;
                if (entry.isIntersecting) {
                    $(`#slideBullets p`).removeClass('activeBullet')
                    $(`#slideBullets p:nth-child(${$(target).index() + 1})`).addClass('activeBullet')
                    if (!entry.target.classList.contains("stress-free-carousel-slide")) {
                        $("#slideBullets").addClass('show')
                        if (window.innerWidth < 767 && entry.intersectionRatio > 0.5) {
                            $('.swipe-content').css('scroll-snap-type', 'y mandatory')
                        }
                    } else {
                        $("#slideBullets").removeClass('show')
                        if (window.innerWidth < 767 && entry.intersectionRatio > 0.5) { 
                            $('.swipe-content').css('scroll-snap-type', 'none')
                        }
                    }
                }
            });
        };
        // start observing elements
        const slidersObserver = new IntersectionObserver(callback, options);
        swipePages.forEach((el) => slidersObserver.observe(el));
    }

    // tabs on click to switch between tabs and to scroll to initial tab's content top position

    const tabs = document.querySelectorAll(".tabtab");
    const tabContents = document.querySelectorAll(".tab-content");
    const tabsConteiner = document.querySelector(".tab-content-container")
    const swipeContent = document.querySelector(".swipe-content")

    const seemoreBtns = document.querySelectorAll(".btn-seemore");

    const numberOfBulletForEachTab = [8, 6, 7, 6, 5];


    tabs.forEach((tab, index) => tab.addEventListener("click", function() {
        // console.log(numberOfBulletForEachTab[index]);
        tabs.forEach(tab => tab.classList.remove("active"));
        tabContents.forEach(c => c.classList.remove("show-content"));

        const contentId = this.dataset.content;

        this.classList.add("active");

        document.querySelector(`.tab-content[data-content="${contentId}"]`).classList.add("show-content");

        // number of bullets of page change
        $("#slideBullets p").remove();
        for (let i = 0; i < numberOfBulletForEachTab[index]; i++) {
            $('#slideBullets').append("<p>•</p>");
        }
        // $( "#slideBullets p:first-child").addClass('activeBullet')


        // to scroll to initial tab's content postion
        setTimeout(function() {
            tabsConteiner.scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            });
        }, 300)
    }));


    seemoreBtns.forEach(btn => btn.addEventListener("click", function() {
        // console.log(this.classList.contains('seeMorePlayers'))
        // tabsConteiner.scrollIntoView({ behavior: 'smooth', block: 'start'});
        tabs.forEach(tab => tab.classList.remove("active"));
        if (this.classList.contains('seeMorePlayers')) {
            // simulate click
            document.querySelector('#tab2').click();
            document.querySelector('.tabs2').click();
            document.querySelector('.tabs2').classList.add("active");
        } else if (this.classList.contains('seeMoreTreasurers')) {
            document.querySelector('#tab3').click();
            document.querySelector('.tabs3').click();
            document.querySelector('.tabs3').classList.add("active");
        } else if (this.classList.contains('seeMoreMembership')) {
            document.querySelector('#tab4').click();
            document.querySelector('.tabs4').click();
            document.querySelector('.tabs4').classList.add("active");
        } else if (this.classList.contains('seeMoreJunior')) {
            document.querySelector('#tab5').click();
            document.querySelector('.tabs5').click();
            document.querySelector('.tabs5').classList.add("active");
        } else if (this.classList.contains('seeMoreCoaches')) {
            document.querySelector('#tab1').click();
            document.querySelector('.tabs1').click();
            document.querySelector('.tabs1').classList.add("active");
        }
        // tabsConteiner.scrollIntoView({ behavior: 'smooth', block: 'start'});
    }))

    // jQuery counterUp
    $('.counter').counterUp({
        delay: 10,
        time: 2000
    });

    // MoreLEss button features
    if (window.innerWidth < 991) {
        $('.moreless-button').text("=")
        $('.moreless-button').css("font-size", "34px")
    } else {
        $('.moreless-button').text("Read more")
        $('.moreless-button').css("padding", "6px 24px !important");
        $('.moreless-button').css("font-size", "14px");
    }
    window.addEventListener('resize', function() {
        if (window.innerWidth < 991) {
            $('.moreless-button').text("=")
            $('.moreless-button').css("font-size", "34px")
        } else {
            $('.moreless-button').text("Read more")
            $('.moreless-button').css("padding", "6px 24px !important");
            $('.moreless-button').css("font-size", "14px");
        }
    }, true);

    $('.moreless-button').click(function() {
        $(this).closest('.scroll-page-textside').find('.moretext').slideToggle(300);
        if (window.innerWidth < 991) {
            if ($(this).text() == "=") {
                $(this).text("✖");
                $(this).css("padding", "0px !important");
                $(this).css("font-size", "14px");
                $(this).closest('.scroll-page-textside').find(".btn-howtouseit").removeClass("hidden");

                $(this).closest('.swipe-slide').find(".scroll-page-textside").addClass("extended");
            } else {
                $(this).text("=");
                $(this).css("font-size", "34px");
                $(this).closest('.scroll-page-textside').find(".btn-howtouseit").addClass("hidden");

                $(this).closest('.swipe-slide').find(".scroll-page-textside").removeClass("extended");

            }
        } else {
            if ($(this).text() == "Read more") {
                $(this).text("✖");
                $(this).css("padding", "6px 8px");
                $(this).closest('div .scroll-page-textside').find(".btn-howtouseit").removeClass("hidden");
            } else {
                $(this).text("Read more");
                $(this).css("padding", "6px 24px");
                $(this).closest('div .scroll-page-textside').find(".btn-howtouseit").addClass("hidden");
            }
        }

    });


    // site images lazy load
    [].forEach.call(document.querySelectorAll('img[data-src]'), function(img) {
        img.setAttribute('src', img.getAttribute('data-src'));
        img.onload = function() {
            img.removeAttribute('data-src');
        };
    });

    // Sticky Navbar
    $(".swipe-content").scroll(function() {
        if (swipeContent.scrollTop > 0) {
            $('.navbar').addClass('nav-sticky');

        } else {
            $('.navbar').removeClass('nav-sticky');
        }
    });

    $('.navbar-toggler').on('click', function() {
        if ($(this).hasClass('collapsed')) {
            $('.navbar').addClass('navbar-mobile');
            $('.btn-main-mobile').css('visibility', 'hidden');
        } else {
            $('.navbar').removeClass('navbar-mobile');
            $('.btn-main-mobile').css('visibility', 'visible');
        }
    });


    // Main head carousel
    let swiper = new Swiper('.mySwiper0', {
        direction: 'horizontal',
        effect: "cube",
        loop: true,
        centeredSlides: true,
        autoplay: {
            delay: 3000,
        },
        cubeEffect: {
            shadow: false,
            slideShadows: false,
        },

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },

    });
    swiper.update();

    // "What our users have been..." carousel
    let swipercarousel = new Swiper('.swiper-carousel', {
        loop: true,
        direction: 'horizontal',
        lazy: true,
        slidesPerView: 4,
        spaceBetween: 40,
        slideToClickedSlide: true,

        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-carousel-pagination",
        },
        breakpoints: {
            300: {
                slidesPerView: 1,
                spaceBetween: 40,
            },
            400: {
                slidesPerView: 1.2,
                spaceBetween: 40,
            },
            540: {
                slidesPerView: 1.8,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 2.4,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            1140: {
                slidesPerView: 2.5,
                spaceBetween: 30,
            },
            1366: {
                slidesPerView: 3.2,
                spaceBetween: 40,
            },
            1440: {
                slidesPerView: 3.1,
                spaceBetween: 40,
            },
            1920: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
        },
    });
    swipercarousel.update();
});

// Modal Video feature
$(document).ready(function() {
    var $videoSrc;
    $('.btn-play').click(function() {
        $videoSrc = $(this).data("src");
    });
    console.log($videoSrc);

    $('#videoModal').on('shown.bs.modal', function(e) {
        $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
    })

    $('#videoModal').on('hide.bs.modal', function(e) {
        $("#video").attr('src', $videoSrc);
    })
});